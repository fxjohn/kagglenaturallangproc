
# coding: utf-8

# In[3]:

import pandas as pd
import os

#Make this directory your working directory(or the directory which has your result files)
files = os.listdir("C:\Users\Nightshadow")

#I make the intermediate dataframe empty just so that it's available outside the for-loop
index = range(0,66292)
columns = ['PhraseId', 'Sentiment']
intermediate = pd.DataFrame(0, index=index, columns=columns)
base = pd.DataFrame(0, index=index, columns=columns)
first = True
for item in files:
    if "result" not in item: continue
    if ".csv" not in item: continue
    single_file_results = pd.read_csv(item, delimiter=',')
    if(first):
        intermediate = single_file_results.copy()
        base = single_file_results.copy()
        first = False
        
    else:
        intermediate = intermediate.merge(single_file_results, on='PhraseId')
    
del intermediate['PhraseId']
mean = intermediate.mean(axis=1)
mean = mean.astype(int)
mode = intermediate.mode(axis=1)

median = intermediate.median(axis=1)
median = median.astype(int)



# In[6]:

mean_result = base
mean_result['Sentiment'] = mean

#mode_result = base
#mode_result['Sentiment'] = mode

median_result = base
median_result['Sentiment'] = median
print mean_result
mean_result.to_csv("meanEnsemble.csv", index=False)
#mode_result.to_csv("modeEnsemble.csv", index=False)
median_result.to_csv("medianEnsemble.csv", index=False)


# In[ ]:



