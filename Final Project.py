
# coding: utf-8

# In[5]:

import nltk
import pandas as pd
import numpy as np
import nltk.classify as nbc
import re
import csv

data = pd.read_csv("train.tsv",
                   delimiter='\t')
zeroes = data[data['Sentiment']==0]
ones = data[data['Sentiment']==1]
twos = data[data['Sentiment']==2]
threes = data[data['Sentiment']==3]
fours = data[data['Sentiment']==4]

positives = threes.append(fours)
negatives = ones.append(zeroes)


# In[7]:

#twitter sentiment example http://www.laurentluce.com/posts/twitter-sentiment-analysis-using-python-and-nltk/
import random
from nltk.corpus import stopwords
from sklearn import cross_validation
filename = "naive_result_nltk_stop.csv"


pairs = data[['Phrase', 'Sentiment']].as_matrix()


minRange = 0
maxRange = 20000
test = pairs[minRange:maxRange]
phrazes = []
for (words, sentiment) in test:
    words_filtered = [e.lower() for e in words.split() if len(e) >= 3]
    phrazes.append((words_filtered, sentiment))

def get_words_in_phrazes(phrazes):
    all_words = []
    for (words, sentiment) in phrazes:
        all_words.extend(words)
    return all_words

def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    word_features = wordlist.keys()
    return word_features

word_features = get_word_features(get_words_in_phrazes(phrazes))

nltk_filtered_words = [w for w in word_features if not w in stopwords.words('english')]
#mys_stop_words = pd.read_csv("mysStopWords.csv", header=0)
#nltk_filtered_words = [w for w in word_features if w not in nltk_filtered_words]
def extract_features(document):
    document_words = set(document)
    features = {}
    for word in nltk_filtered_words:
        features['contains(%s)' % word] = (word in document_words)
    return features

training_set = nltk.classify.apply_features(extract_features, phrazes)

classifier = nltk.NaiveBayesClassifier.train(training_set)

#test = pd.read_csv("test.tsv",
#                   delimiter='\t')
#result = test[['PhraseId','Phrase']]
#result = result.rename(columns={'Phrase': 'Sentiment'})

#for i, row in result.iterrows():
#        val = classifier.classify(extract_features(row['Sentiment'].split()))
#        result.set_value(i, 'Sentiment', val)
#        if(i % 1000 == 0):
#            print "Processed " + repr(i) + " phrases"

#result.to_csv(filename, index=False)
print "Processing Complete"


# In[67]:

from __future__ import division
from scipy import stats

data = data.reindex(np.random.permutation(data.index))

pairs = data[['Phrase', 'Sentiment']].as_matrix()

cv_test = pairs[0:20000]
cv_train1 = pairs[20001:40000]
cv_train2 = pairs[40001:60000]
cv_train3 = pairs[60001:80000]
cv_train4 = pairs[80001:100000]

def run_test(train, test):
    phrazes = []
    for (words, sentiment) in train:
        words_filtered = [e.lower() for e in words.split() if len(e) >= 3]
        phrazes.append((words_filtered, sentiment))
    word_features = get_word_features(get_words_in_phrazes(phrazes))
    nltk_filtered_words = [w for w in word_features if not w in stopwords.words('english')]
    training_set = nltk.classify.apply_features(extract_features, phrazes)
    classifier = nltk.NaiveBayesClassifier.train(training_set)

    results = []
    count = 0
    for row in cv_test:
        val = classifier.classify(extract_features(row[0].split()))
        if (row[1] == val):
            results.append(True)
        else:
            results.append(False)
        if(count % 1000 == 0):
            print "Processed " + repr(count) + " phrases"
        count = count + 1
    return np.count_nonzero(results) / len(results)
    
correct_results = []
correct_results.append(run_test(cv_train1, cv_test))
correct_results.append(run_test(cv_train2, cv_test))
correct_results.append(run_test(cv_train3, cv_test))
correct_results.append(run_test(cv_train4, cv_test))

print "Mean: " + repr(np.mean(correct_results))
print "Median: " + repr(np.median(correct_results))
print "Mode: " + repr(stats.mode(correct_results))

print "Std:" + repr(np.std(correct_results))


# In[ ]:



