# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 21:45:27 2015
@author: johnjmar
"""
import re
import gc
import logging
import pickle
import numpy as np
import pandas as pd
import pylab as pl
import matplotlib.pyplot as plt

from random import sample
from scipy import stats
from multiprocessing import Pool
from nltk.corpus import stopwords
from gensim.models import word2vec#, doc2vec

from sklearn import svm, cross_validation, decomposition
from sklearn.preprocessing import StandardScaler
from sklearn.metrics \
    import confusion_matrix, classification_report, roc_curve, auc
from sklearn.linear_model import SGDClassifier
from sklearn.feature_extraction.text \
    import CountVectorizer, TfidfVectorizer, TfidfTransformer, HashingVectorizer
from sklearn.feature_selection import SelectKBest, chi2

_SPC_=" "
NON_ALPHA_REGEX  = re.compile("[^\w,']") #REMOVE non-alpha chars except , and '

NLTK_STOPS = list(stopwords.words("english"))
MPF=7  #PARALLELIZATION FACTOR

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#######################################################################
def BREAK():
    assert False

def preprocess(reviews, regex, i):
    phrases = reviews[i].apply(str.lower)
    if REMOVE_NON_ALPHA:
        phrases = phrases.apply(lambda x: NON_ALPHA_REGEX.sub(" ", x))    
    phrases = phrases.apply(lambda x: stops_regex.sub(" ",_SPC_+x+_SPC_))    
    return phrases.apply(lambda x: " ".join(x.split()))

def find_2val_singles_distrib(p, p2_singles, i):
    d={}
    _x_=_SPC_+_SPC_.join(p[i]).lower()+_SPC_ #Flatten and pad it
    for w in p2_singles:
        d[w]=_x_.count(_SPC_+w+_SPC_)
    return d

def plot_confusion_matrix(cm, title='Confusion matrix', cmap=plt.cm.Greens):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

def accuracy(predicted, actual):
    prediction_array = np.equal(predicted, actual)
    return float(np.count_nonzero(prediction_array))/float(len(predicted))

def cross_validate(kf_set, i):
    X=kf_set['train'][i][0]
    y=kf_set['train'][i][1]

    Xtest = kf_set['test'][i][0]
    ytest = kf_set['test'][i][1]
    model=kf_set['model'][i]

    print "... fitting model %s"%i
    model.fit(X, y)

    print "... predicting using model %s"%i
    p1 = model.predict(Xtest)
    p2 = model.predict(X)
    acc1=accuracy(p1, ytest)
    acc2=accuracy(p2, y)
    cm1=confusion_matrix(ytest, p1)
    cm2=confusion_matrix(y, p2)
    clssrep = classification_report(ytest, p1)
    print clssrep
    return [p1, p2, [acc1, acc2], [cm1, cm2], model, clssrep]

def getAvgFeatureVecs(phrases, model, num_feats, i):
    counter = 0.
    indx2wrd = set(model.index2word)
    reviewFeatureVecs = np.zeros((len(phrases[i]),num_feats), dtype="float32")
    for p in phrases[i]:
       reviewFeatureVecs[counter] = makeFeatureVec(p, model, num_feats, indx2wrd)
       counter += 1.

    return reviewFeatureVecs

def makeFeatureVec(words, model, num_features, i2w):
    featureVec = np.zeros((num_features,),dtype="float32")
    nw = 0.
    for w in words:
        if w in i2w:
            nw += 1.
            featureVec = np.add(featureVec, model[w])

    featureVec = np.divide(featureVec, nw)
    return featureVec

def model_fit (model, xvalidate, parallel, X, y):
    if xvalidate:
        kf_set={'model':[], 'train':[], 'test':[]}
        kf = cross_validation.StratifiedKFold(y, n_folds=MPF)
        for train_index, test_index in kf:
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]

            kf_set['model'].append(model)
            kf_set['train'].append([X_train,y_train])
            kf_set['test'].append([X_test,y_test])

        print "... starting cross validation check"
        if parallel:
            pool = Pool(processes=MPF)
            results = [pool.apply_async(cross_validate,
                                            args=(kf_set,x)) for x in range(MPF)]
            pool_results = [x.get() for x in results]
            pool.close()
            del(pool,x,results)
            print "DONE"
        else:
            pool_results = [ cross_validate(kf_set,x) for x in range(MPF) ]

        results = pd.DataFrame(pool_results, columns=['tst_prd','trn_prd', 'acc', 'cm', 'm', 'r'])
        kf_set = pd.DataFrame(kf_set)
        return [kf_set, results]
    else:
       print "... fitting model %s"%model
       model.fit(X, y)
       print "... predicting using model %s"%model
       prd=model.predict(X)
       acc=accuracy(prd, y)
       cm=confusion_matrix(y, prd)
       rep=classification_report(y, prd)
       print rep
       return [prd, acc, cm, model, rep]

def plot_roc(fpr, tpr, roc_auc, i):
    #pl.clf()
    pl.plot(fpr, tpr, label=i)
    pl.plot([0, 1], [0, 1], 'k--')
    pl.xlim([0.0, 1.0])
    pl.ylim([0.0, 1.0])
    pl.xlabel('False Positive Rate - FPR')
    pl.ylabel('True Positive Rate - TPR')
    pl.title('Receiver Operating Characteristic (ROC)')
    pl.legend(loc=4)
    pl.show()

def run_PCA(num_feats, scaled_feats): #PCA --- FEAT REDUCTION
    print "\tat PCA ... takes a while ... "
    pca = decomposition.TruncatedSVD(n_components=num_feats/3) #300->100
    num = len(scaled_feats)
    if MPF <= num:  return
    pool = Pool(processes=num)
    results = [pool.apply_async(quick_parallel,
                                args=(pca,'xfrm',scaled_feats,i)) for i in range(num)]
    pool_results = [x.get() for x in results]
    pool.close()
    del(pool,x,results)
    print "\tDONE"
    return pool_results

def pickle_it(struct, name, ext='.pkl'):
    out_file = open(name+ext, 'wb')
    pickle.dump(struct, out_file)
    out_file.close()
    print "\nDONE pickling %s%s\n"%(name, ext)

def unpickle_it(file_): #FOR LOADING
    in_file = open(file_, 'rb', 1)
    struct = pickle.load(in_file)
    in_file.close()
    return struct

def quick_parallel(model, func, data, i):
    if func == 'xfrm':
        tmp = model.fit_transform(data[i]) #pca.fit_transform()
        print np.cumsum(tmp.explained_variance_ratio_)
        return tmp


#######################################################################

############## READ
print "Reading file"
raw_train = pd.read_csv("../Desktop/DMspring2015/train.tsv", header=0, delimiter="\t", quoting=3)
num_words = pd.DataFrame([len(x.split()) for x in raw_train.Phrase], columns=["numWords"])
raw_train = raw_train.join(num_words)
original = raw_train

p = {}
for i in range(5): p[i] = raw_train[raw_train.Sentiment==i].Phrase #divide per sentiment
p2_singles = [ x.lower() for x in p[2] if len(x.split()) == 1 ] #get neutral words

smpl = sample( xrange(len(raw_train)), len(raw_train)/(n*2) )
raw_train = raw_train.iloc[sorted(smpl)]                #--- FOR TESTING PURPOSES
data_size=len(raw_train)

############## ANALYZE
#MULTIPROCESS THAT SHITE
print "Analyzing data"
pool = Pool(processes=5)
results = [pool.apply_async(find_2val_singles_distrib, args=(p, p2_singles, True, x)) for x in range(5)]
pool_results = [x.get() for x in results]
pool.close()
del(pool,x,results)
gc.collect()

#NEUTRAL SINGLE WRD DISTRB
neutral_tmp=pd.DataFrame(pool_results).T
neutral_tmp.describe()
total=neutral_tmp.sum(1)
#plt.scatter(neutral_tmp[2], neutral_tmp[2]-neutral_tmp[0])
black1 = list(neutral_tmp[(np.abs(stats.zscore(neutral_tmp)) > 0.67).all(axis=1)].index)

neutral_log=np.log(neutral_tmp+1e-10)
neutral_log.describe()
#plt.scatter(neutral_log[2], neutral_log[2]-neutral_log[0])
neutral_log_mean=neutral_log.mean(axis=0)
neutral_log_sd=neutral_log.std(axis=0)
NL_centered=neutral_log-neutral_log_mean
#d_1sd=pd.DataFrame([neutral_log_mean+neutral_log_sd,neutral_log_mean-neutral_log_sd]).T
#d_1sd.columns = (["max","min"])
black2a = list(neutral_log[np.abs(NL_centered)>neutral_log_sd].dropna().index) #drops if ALL NaN
black2b = list(neutral_log[np.abs(NL_centered)>neutral_log_sd].dropna(how='any', thresh=3).index)
black2c = list(neutral_log[np.abs(NL_centered)>neutral_log_sd].dropna(how='any', thresh=4).index)
black2d = list(neutral_log[(np.abs(stats.zscore(neutral_log)) > 0.67).all(axis=1)].index)
#black2e = list(neutral_log[(np.abs(stats.zscore(neutral_log)) >  1.0).all(axis=1)].index) #same as 2a

#MAKE REGEX
black_list = black2b
stops_regex = re.compile(r"\b(?:%s)\b" % "|".join(map(re.escape, black_list)))
no_2singles_regex = re.compile(r"\b(?:%s)\b" % "|".join(map(re.escape, p2_singles)))


print "Preprocessing data"
#MULTIPROCESS THAT SHITE AGAIN
phrs_chunks = np.array_split(raw_train.Phrase,MPF)

#STOPS REGEX
pool = Pool(processes=MPF)
results = [pool.apply_async(preprocess, args=(phrs_chunks,stops_regex, True, x)) for x in range(MPF)]
phrases = [x.get() for x in results]
pool.close()
del(pool,x,results)
gc.collect()

resulting_set = pd.DataFrame(pd.concat(phrases)).join(raw_train.Sentiment)
resulting_set = resulting_set[resulting_set.Phrase!=""].drop_duplicates()
num_words = pd.DataFrame([len(x.split()) for x in resulting_set.Phrase], columns=["numWords"])
clean_set = resulting_set.reset_index().join(num_words).set_index('index')

#ALL NEUTRAL SINGLE WORDS REGEX
pool = Pool(processes=MPF)
results = [pool.apply_async(preprocess, args=(phrs_chunks,no_2singles_regex, False, x)) for x in range(MPF)]
phrases = [x.get() for x in results]
pool.close()
del(pool,x,results)
gc.collect()

resulting_set = pd.DataFrame(pd.concat(phrases)).join(raw_train.Sentiment)
resulting_set = resulting_set[resulting_set.Phrase!=""].drop_duplicates()
num_words = pd.DataFrame([len(x.split()) for x in resulting_set.Phrase], columns=["numWords"])
no_neutrals_set = resulting_set.reset_index().join(num_words).set_index('index')


##CHECK --- should be all zeroes
#pool = Pool(processes=5)
#results = [pool.apply_async(find_2val_singles_distrib,
#                            args=(phrases,black_list,x)) for x in range(5)]
#pool_results = [x.get() for x in results]
#pool.close()
#d = pd.DataFrame(d).T.sum(1)
#check = d[d != 0 ]
#resulting_set = pd.DataFrame([inner for outer in phrases for inner in outer],
#                             columns=["Phrase"])
BREAK()
#del(resulting_set)


############## FEATURE REDUCTION/EXTRACTION
num_feats=300
#stops   = [None, black_list, p2_singles]#, NLTK_STOPS]
#names   = ['none', 'black_list', 'all_neutral']
scaler  = [StandardScaler(), StandardScaler(with_std=False)]
stops = [p2_singles]
names = ['all_neutral']
a = ['word', 'char_wb']
ngrm = [(1,1),(1,2),(2,2),(1,3)]

for i, stop_wrds in enumerate(stops):
    feats={}
    gc.collect()

    if names[i] == 'all_neutral':
        test_set = no_neutrals_set
    else:
        test_set = clean_set

    #BAG OF WORDS
    print "at BAG OF WORDS - stops:%s"%names[i]
    bow = CountVectorizer(analyzer=a[0], tokenizer=None, preprocessor=None, stop_words=stop_wrds,
                          ngram_range=ngrm[3], max_features=num_feats)
    feats['bow'] = {'orig' : bow.fit_transform(test_set.Phrase)}
    print bow
    feats['bow']['struct'] = bow
    feats['bow']['scl0'] = scaler[0].fit_transform(feats['bow']['orig'].toarray())
    #feats['bow']['scl1'] = scaler[1].fit_transform(feats['bow']['orig'].toarray())

    #TF-IDF
    print "at TF-IDF1 - stops:%s"%names[i]
    tf_idf = TfidfVectorizer(analyzer=a[0], tokenizer=None, preprocessor=None, stop_words=stop_wrds,
                             ngram_range=ngrm[3], max_features=num_feats)
    feats['tf'] = {'orig' : tf_idf.fit_transform(test_set.Phrase)}
    print tf_idf
    feats['tf']['struct'] = tf_idf
    feats['tf']['scl0'] = scaler[0].fit_transform(feats['tf']['orig'].toarray())
    #feats['tf']['scl1'] = scaler[1].fit_transform(feats['tf']['orig'].toarray())

    #PCA - 1st run
    #feats['bow']['pca0'], feats['bow']['pca1'], feats['tf']['pca0'] = \
    #        run_PCA(num_feats, [feats['bow']['scl0'],feats['bow']['scl1'],feats['tf']['scl0']])
    feats['bow']['pca0'], feats['tf']['pca0'] = run_PCA(num_feats, [feats['bow']['scl0'], feats['tf']['scl0']])
    gc.collect()

    #TFIDF<<BOW = TfidfTransformer(use_idf=False)
    print "at TF-IDF2 - stops:%s"%names[i]
    tf1 = TfidfTransformer()
    feats['tf1'] = {'orig' : tf1.fit_transform(feats['bow']['orig'])}
    print tf1
    feats['tf1']['struct'] = tf1
    feats['tf1']['scl0'] = scaler[0].fit_transform(feats['tf1']['orig'].toarray())
    #feats['tf1']['scl1'] = scaler[1].fit_transform(feats['tf1']['orig'].toarray())

    #PCA - 2nd run
    feats['tf1']['pca0'] = run_PCA(num_feats, [feats['tf1']['scl0']])
    gc.collect()

    #HASHING VECTZR
    hsv = HashingVectorizer(analyzer='word', tokenizer=None, preprocessor=None, stop_words=stop_wrds, 
                            ngram_range=ngrm[3], non_negative=True, n_features=num_feats) #positive needed for chi_sq
    feats['hsv'] = {'orig' : hsv.fit_transform(test_set.Phrase)}
    print hsv
    feats['hsv']['struct'] = hsv
    feats['hsv']['chi2'] = SelectKBest(chi2, 100)
    feats['hsv']['redx'] = feats['hsv']['chi2'].fit_transform(feats['hsv']['orig'], test_set.Sentiment.values)
  
    feats['y_clean'] = test_set.Sentiment.values
    for f in feats:
        pickle_it(feats[f], "%s_set_%s_feats_%s"%(f+'_1_3ngrm', len(test_set), num_feats))

##WORD2VEC --- use raw data
feats={}
name="w2v"
print "at W2V"
min_wrd_n = 1        # Minimum word count, 1=ALL
context = 50         # Context window size
downsampling = 1e-3  # Downsample setting for frequent words
w2v = word2vec.Word2Vec(original.Phrase, workers=MPF, size=num_feats, min_count=min_wrd_n,
                        window=context, sample=downsampling)
#w2v.init_sims(replace=True) #Turn off when needed to retrain.
phrs_chunks = np.array_split(original.Phrase, MPF)
pool = Pool(processes=MPF)
results = [pool.apply_async(getAvgFeatureVecs, args=(phrs_chunks,w2v,num_feats,x)) for x in range(MPF)]
feat_vects = [x.get() for x in results]
pool.close()
del(pool,x,results)
gc.collect()

feats['w2v'] = { 'orig' : np.concatenate(feat_vects) }
feats['w2v']['struct'] = w2v
feats['y_raw'] = original.Sentiment.values
pickle_it([feats['w2v']['orig'],feats['y_raw'],feats['w2v']['struct']], 
          "%s_set_%s_feats_%s__2"%(name, len(original), num_feats))
gc.collect()

feats['w2v']['scl'] = scaler[0].fit_transform(feats['w2v']['orig'])
feats['w2v']['pca'] = run_PCA(num_feats, [feats['w2v']['scl']])
pickle_it([feats['w2v']['scl'],feats['w2v']['pca']], 
          "%s_set_%s_feats_%s__0"%(name, len(original), num_feats))
del(feats['w2v']['scl'],feats['w2v']['pca'])          
gc.collect()

#feats['w2v']['scl'] = scaler[1].fit_transform(feats['w2v']['orig'])
#feats['w2v']['pca'] = run_PCA(num_feats, [feats['w2v']['scl']])
#pickle_it([feats['w2v']['scl'],feats['w2v']['pca']], 
#          "%s_set_%s_feats_%s__1"%(name, len(original), num_feats))          
#gc.collect()          
#          
#print scaled_feats, np.cumsum(pca.explained_variance_ratio_)
#DOC2VEC --- should use raw data?
#print "at D2V"
#d2v = doc2vec.Doc2Vec( workers=n, size=num_feats,
#                       min_count=min_wrd_n, window=context, sample=downsampling)
#d2v.init_sims(replace=True)

############## MODELING
print "modeling with %s features"%num_feats
#SVM RBF
svm_rbf = [ svm.SVC(kernel='rbf', cache_size=1000, class_weight='auto', gamma=0.5, C=0.5) ]
svm_rbf.append( svm.NuSVC(kernel='rbf', cache_size=1000, nu=0.115) )

#SVM LINEAR AKA SGD ---DO NOT PARALLELIZE, IE USE WITH POOL!
sgd = [ SGDClassifier(alpha=.0001, penalty="elasticnet", n_jobs=MPF, loss='modified_huber',
                      class_weight='auto', shuffle=True, n_iter=num_feats) ]
sgd.append( SGDClassifier(alpha=.0001, n_jobs=MPF, class_weight='auto', shuffle=True,
                          n_iter=num_feats, loss='modified_huber', penalty="l2" ) )
sgd.append( SGDClassifier(alpha=.0001, n_jobs=MPF, class_weight='auto', loss='modified_huber',
                          shuffle=True, n_iter=num_feats) )
model=svm_rbf[0]
featurizer='bow'
transform='scl0'
xvalidate = False
parallel  = False
X=feats[featurizer][transform]
if featurizer == 'w2v':
    y=original.Sentiment.values
else:
    y=test_set.Sentiment.values
print "using %s features and\n%s"%(featurizer, model)
results = model_fit(model, xvalidate, parallel, X, y)
results

plt.close()
#Plot normalize Confusion Matrix
cm=results[2]
cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
plot_confusion_matrix(cm_normalized)

print results[3]
print results[4]

#Compute ROC curve and area the curve
plt.close()
true=scaler[0].fit_transform(y)
pred=scaler[0].fit_transform(results[0])
for i in range(5):
    fpr, tpr, thresholds = roc_curve(true, pred, pos_label=i)
    roc_auc = auc(fpr, tpr)
    print "AUC %s: %s"%(i, roc_auc)
    plot_roc(fpr, tpr, roc_auc, i)

BREAK()
#fpr, tpr, thresholds = roc_curve(np.array(list(true==3)).astype(int),
#                                 np.array(list(pred==3)).astype(int))


############## TEST
featurizer = bow
print "Using model on test data"
raw_test = pd.read_csv("Desktop/DMspring2015/test.tsv", header=0, delimiter="\t",
                       quoting=3)

reviews = np.array_split(raw_test.Phrase,n)
pool = Pool(processes=MPF)
results = [pool.apply_async(preprocess, args=(reviews,stops_regex,x)) for x in range(MPF)]
phrases = [x.get() for x in results]
pool.close()
del(pool,x,results)

resulting_set = pd.DataFrame(pd.concat(phrases)).join(raw_train.Sentiment)
resulting_set = resulting_set[resulting_set.Phrase!=""].drop_duplicates()
num_words = pd.DataFrame([len(x.split()) for x in resulting_set.Phrase], columns=["numWords"])
clean_test = resulting_set.reset_index().join(num_words).set_index('index')
del(resulting_set)

if featurizer == w2v:
    pool = Pool(processes=MPF)
    results = [pool.apply_async(getAvgFeatureVecs, args=(phrs_chunks,w2v,num_feats,x)) for x in range(MPF)]
    feat_vecs = [x.get() for x in results]
    pool.close()
    del(pool,x,results)

    feats = np.concatenate(feat_vecs)
    clean_feats = scaler.fit_transform(feats)
else:
    feats = featurizer.fit_transform(clean_test.Phrase)
    clean_feats = scaler.fit_transform(feats.toarray())

clean_pca = pca.fit_transform(clean_feats)
del(clean_feats)
clean_pred  = model.predict(clean_pca)

clean_pred = pd.DataFrame(clean_pred, columns=["Sentiment"])
clean_test = clean_test.join(clean_pred)

finalized_test = raw_test.join(clean_test.Sentiment)
finalized_test.Sentiment[np.isnan(finalized_test.Sentiment)] = 2 # Replace 'nan's
finalized_test = finalized_test.drop(['SentenceId','Phrase'], axis=1)
#finalized_test.Sentiment = finalized_test.Sentiment.astype(int)
#finalized_test.to_csv('sentiments.csv', index_label=False, index=False)

############## GRAPHS
#from mpl_toolkits.mplot3d import proj3d
#gross=pd.crosstab(raw_train.Sentiment, raw_train.numWords)
#gross.plot(kind='bar', stacked=True, legend='reverse')
#
#net=pd.crosstab(clean_set.Sentiment, clean_set.numWords)
#net.plot(kind='bar', stacked=True, legend='reverse')\

##2D/3D representation
#X=features['pca']
#y=raw_train.Sentiment
#df=pd.DataFrame(X).join(y.reset_index())
#c=['b','g','y','orange','r']
#m=['x','<','o','>','+']
#a=[0.6,0.4,0.2,0.4,0.6]
#
#for i in range(5):
#    img = df[df.Sentiment==i]
#    plt.scatter(img.iloc[:, 0], img.iloc[:, 1], c=c[i], label=i, alpha=a[i],
#                marker=m[i])
#plt.xticks(())
#plt.yticks(())
#plt.legend()
#
#plt.close()
#fig = plt.figure()
#ax = fig.gca(projection='3d', alpha=0.5)
#for i in range(5):
#    img = df[df.Sentiment==i]
#    ax.scatter(img.iloc[:, 0], img.iloc[:, 1], img.iloc[:, 2], c=c[i], label=i,
#               alpha=a[i], marker=m[i])
#plt.xticks(())
#plt.yticks(())
#plt.legend()
#
#plt.close()
